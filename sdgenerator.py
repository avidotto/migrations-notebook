import arcpy
import ntpath
import os

arcpy.env.overwriteOutput = True

mxd_path = arcpy.GetParameterAsText(0)
mxd_path = 'C:\\Temp\\MT.mxd'
commom_sdddraft_path = arcpy.GetParameterAsText(1)
if not commom_sdddraft_path.endswith('\\'):
    commom_sdddraft_path = commom_sdddraft_path + '\\'
commom_sdddraft_path = 'C:\\Temp\\'

mapDoc = arcpy.mapping.MapDocument(mxd_path)
service = ntpath.basename(os.path.splitext(mxd_path)[0])

sddraft = commom_sdddraft_path + service + '.sddraft'
sd = commom_sdddraft_path + service + '.sd'
tags = 'wfitem'
description = 'Workflow Item'

arcpy.mapping.CreateMapSDDraft(mapDoc, sddraft, service, 'MY_HOSTED_SERVICES', copy_data_to_server=True, summary=description, tags=tags)
analysis = arcpy.mapping.AnalyzeForSD(sddraft)

if analysis['errors'] == {}:
    arcpy.StageService_server(sddraft, sd)
    print sd
else:
    print 'error'

del mapDoc