import arcpy 
import xml.dom.minidom as DOM  
import os  

workspace = 'C:/Temp/'  
# Reference map document for CreateSDDraft function. 
mapDoc = arcpy.mapping.MapDocument(r'C:\Temp\MT.mxd') 
    
# Create service and sddraft variables for CreateSDDraft function.

sddraft = workspace + 'MT.sddraft'
service = 'FeatureService'

arcpy.mapping.CreateMapSDDraft(mapDoc, sddraft, service, 'MY_HOSTED_SERVICES', summary="test", tags='test')

con = 'My Hosted Services'
sd = workspace + service + '.sd'
doc = DOM.parse(sddraft)

# Read the sddraft xml.
doc = DOM.parse(sddraft) 

# Change service from map service to feature service
typeNames = doc.getElementsByTagName('TypeName') 

for typeName in typeNames:     
    # Get the TypeName we want to disable. 
    if typeName.firstChild.data == "MapServer":
        typeName.firstChild.data = "FeatureServer"  

#Turn off caching
configProps = doc.getElementsByTagName('ConfigurationProperties')[0] 
propArray = configProps.firstChild
propSets = propArray.childNodes 
for propSet in propSets:
    keyValues = propSet.childNodes
    for keyValue in keyValues:
        if keyValue.tagName == 'Key':
            if keyValue.firstChild.data == "isCached":
                # turn on caching
                keyValue.nextSibling.firstChild.data = "false" 

#Turn on feature access capabilities
configProps = doc.getElementsByTagName('Info')[0]
propArray = configProps.firstChild
propSets = propArray.childNodes
for propSet in propSets:
    keyValues = propSet.childNodes
    for keyValue in keyValues:
        if keyValue.tagName == 'Key':
            if keyValue.firstChild.data == "WebCapabilities":
                # turn on caching
                keyValue.nextSibling.firstChild.data = "Query,Create,Update,Delete,Uploads,Editing"

with open(sddraft, 'w+') as f:
    doc.writexml( f )

analysis = arcpy.mapping.AnalyzeForSD(outXml) 

#print analysis 

arcpy.SignOutFromPortal_server()
arcpy.SignInToPortal_server("username","password","http://www.arcgis.com/")
arcpy.StageService_server(outXml, sd)
arcpy.UploadServiceDefinition_server(sd, con)  
