# README #

### Jupyter notebook with ArcGIS migrations. ###

Specifically used to test ArcGIS API for Python performing tasks such as to migrate a portal item to arcgis online and vice-versa.
Each strategy depends on the item type. Some tasks can be performed using sharing portal api (to a hosted item), in the other hand custom geoprocessing when it is a registered gdb item.